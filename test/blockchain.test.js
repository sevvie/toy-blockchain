/* eslint-env node, mocha */
import { expect } from 'chai';
import Blockchain from '../src/Blockchain';

describe('Blockchain', () => {
  it('should create a list with a genesis block', () => {
    const bc = new Blockchain();
    expect(bc.chain.length).to.equal(1);
    expect(bc.chain[0].timestamp).to.equal(1521471558602);
  });

  it('should return the genesis block as the latest block, before adding any new blocks', () => {
    const bc = new Blockchain();
    expect(bc.getLatestBlock().index).to.equal(bc.chain[0].index);
  });

  it('should test the validity of a block', () => {
    const bc = new Blockchain();
    const newBlock = bc.generateNextBlock([{
      sender: '',
      receiver: '',
      amount: 0,
    }]);

    expect(newBlock.index).to.equal(1);
    newBlock.index = 2;
    expect(bc.isValidBlock(newBlock, bc.getLatestBlock())).to.equal(false);
    newBlock.index = 1;

    const oldPHash = newBlock.previousHash;
    newBlock.previousHash = 'xxxxxxx';
    expect(bc.isValidBlock(newBlock, bc.getLatestBlock())).to.equal(false);
    newBlock.previousHash = oldPHash;

    const oldHash = newBlock.hash;
    newBlock.hash = 'xxxxxxx';
    expect(bc.isValidBlock(newBlock, bc.getLatestBlock())).to.equal(false);
    newBlock.hash = oldHash;
    /* eslint no-console: off */
    expect(bc.isValidBlock(newBlock, bc.getLatestBlock())).to.equal(true);
  });

  it('should not add invalid blocks', () => {
    const bc = new Blockchain();
    const newBlock = bc.generateNextBlock([{
      sender: '',
      receiver: '',
      amount: 0,
    }]);
    const invalidBlock = bc.generateNextBlock([{
      sender: '',
      receiver: '',
      amount: 0,
    }]);

    bc.addBlock(newBlock);
    expect(bc.getLatestBlock().index).to.equal(1);

    invalidBlock.index = -1;
    bc.addBlock(invalidBlock);
    expect(bc.getLatestBlock().index).to.not.equal(-1);
  });

  it('should validate the chain', () => {
    const bc = new Blockchain();
    const newBlock = bc.generateNextBlock([{
      sender: '',
      receiver: '',
      amount: 0,
    }]);
    bc.addBlock(newBlock);

    expect(bc.chain.length).to.equal(2);

    let oldHash = bc.chain[0].hash;
    bc.chain[0].hash = 'xxxxxxx';
    expect(bc.isValidChain()).to.equal(false);
    bc.chain[0].hash = oldHash;

    oldHash = bc.chain[1].hash;
    bc.chain[1].hash = 'xxxxxxx';
    expect(bc.isValidChain()).to.equal(false);
    bc.chain[1].hash = oldHash;

    expect(bc.isValidChain()).to.equal(true);
  });
});
