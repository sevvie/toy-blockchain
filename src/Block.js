import CryptoJS from 'crypto-js';

class Block {
  constructor(index, timestamp, previousHash, data) {
    this.index = index;
    this.timestamp = timestamp;
    this.previousHash = previousHash;
    this.data = data;
    this.hash = this.calculateHash();
  }

  calculateHash() {
    return CryptoJS.SHA256(`${this.index}+${this.timestamp}+${this.previousHash}+${this.data}`).toString();
  }

  toJSON() {
    return JSON.stringify({
      index: this.index,
      timestamp: this.timestamp,
      previousHash: this.previousHash,
      data: this.data,
      hash: this.hash,
    });
  }
}

export default Block;
