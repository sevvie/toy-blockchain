import Block from './Block';

class Blockchain {
  constructor() {
    this.chain = [this.createGenesisBlock()];
  }

  createGenesisBlock() {
    return new Block(0, 1521471558602, '', [{
      sender: '',
      receiver: '',
      amount: 0,
    }]);
  }

  addBlock(block) {
    if (this.isValidBlock(block, this.getLatestBlock())) {
      this.chain.push(block);
    }
  }

  getLatestBlock() {
    return this.chain[this.chain.length - 1];
  }

  generateNextBlock(blockData) {
    const previousBlock = this.getLatestBlock();
    const nextIndex = previousBlock.index + 1;
    const nextTimestamp = new Date().getTime() / 1000;
    return new Block(nextIndex, nextTimestamp, previousBlock.hash, blockData);
  }

  /* eslint class-methods-use-this: off, no-console: off */
  isValidBlock(newBlock, previousBlock) {
    if (previousBlock.index + 1 !== newBlock.index) {
      return false;
    } else if (newBlock.previousHash !== previousBlock.hash) {
      return false;
    } else if (newBlock.hash !== newBlock.calculateHash()) {
      return false;
    }
    return true;
  }

  isValidChain() {
    if (this.chain[0].toJSON() !== this.createGenesisBlock().toJSON()) {
      return false;
    }

    let previousBlock = this.chain[0];
    for (let i = 1; i < this.chain.length; i += 1) {
      if (!this.isValidBlock(this.chain[i], previousBlock)) {
        previousBlock = this.chain[i];
        return false;
      }
    }
    return true;
  }
}

export default Blockchain;
