import resolve from 'rollup-plugin-node-resolve';
import babel from 'rollup-plugin-babel';

export default {
  input: 'src/main.js',
  output: {
    format: 'umd',
    file: 'dist/bundle.js',
    name: 'ToyBlockchain',
  },
  plugins: [
    resolve(),
    babel({
      presets: ['env', {
        modules: false,
      }],
      plugins: ['external-helpers'],
      babelrc: false,
    }),
  ],
};
